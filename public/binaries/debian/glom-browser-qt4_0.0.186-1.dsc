Format: 1.0
Source: glom-browser-qt4
Binary: glom-browser-qt4
Architecture: all
Version: 0.0.186-1
Maintainer: Con Zymaris <cybercamera@gmail.com>
Standards-Version: 3.9.6
Build-Depends: debhelper (>= 9)
Package-List:
 glom-browser-qt4 deb contrib/web optional arch=all
Checksums-Sha1:
 efb36fb00ab1fd8e98f3f4afef0556155722811d 1366 glom-browser-qt4_0.0.186-1.tar.gz
Checksums-Sha256:
 dfdde6b2b513153272c57cfa3b10e069c49539233f8154d55d0593a3ea30af87 1366 glom-browser-qt4_0.0.186-1.tar.gz
Files:
 ff368947d3009962017e8f98078fed9b 1366 glom-browser-qt4_0.0.186-1.tar.gz
