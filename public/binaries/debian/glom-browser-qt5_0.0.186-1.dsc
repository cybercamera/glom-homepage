Format: 1.0
Source: glom-browser-qt5
Binary: glom-browser-qt5
Architecture: all
Version: 0.0.186-1
Maintainer: Con Zymaris <cybercamera@gmail.com>
Standards-Version: 3.9.6
Build-Depends: debhelper (>= 9)
Package-List:
 glom-browser-qt5 deb contrib/web optional arch=all
Checksums-Sha1:
 2c979f27e2e240c7ca85485968172daa6db7eb71 1368 glom-browser-qt5_0.0.186-1.tar.gz
Checksums-Sha256:
 13b1faa8edf4f6ec22c943af1edccec8aff0d42ba30d712878315cf78b2a4f46 1368 glom-browser-qt5_0.0.186-1.tar.gz
Files:
 40d11d5e794718d6c4d7a58b480ec849 1368 glom-browser-qt5_0.0.186-1.tar.gz
