Format: 1.0
Source: glom-browser
Binary: glom-browser
Architecture: all
Version: 0.0.186-1
Maintainer: Con Zymaris <cybercamera@gmail.com>
Standards-Version: 3.9.6
Build-Depends: debhelper (>= 9), gambas3-dev (>= 3.16), gambas3-dev (<< 3.99.0), gambas3-gb-args (>= 3.16), gambas3-gb-args (<< 3.99.0), gambas3-gb-image (>= 3.16), gambas3-gb-image (<< 3.99.0), gambas3-gb-form (>= 3.16), gambas3-gb-form (<< 3.99.0), gambas3-gb-data (>= 3.16), gambas3-gb-data (<< 3.99.0), gambas3-gb-db (>= 3.16), gambas3-gb-db (<< 3.99.0), gambas3-gb-db-form (>= 3.16), gambas3-gb-db-form (<< 3.99.0), gambas3-gb-db-sqlite3 (>= 3.16), gambas3-gb-db-sqlite3 (<< 3.99.0), gambas3-gb-desktop (>= 3.16), gambas3-gb-desktop (<< 3.99.0), gambas3-gb-settings (>= 3.16), gambas3-gb-settings (<< 3.99.0), gambas3-gb-term (>= 3.16), gambas3-gb-term (<< 3.99.0), gambas3-gb-form-terminal (>= 3.16), gambas3-gb-form-terminal (<< 3.99.0), gambas3-gb-net (>= 3.16), gambas3-gb-net (<< 3.99.0), gambas3-gb-net-curl (>= 3.16), gambas3-gb-net-curl (<< 3.99.0), gambas3-gb-markdown (>= 3.16), gambas3-gb-markdown (<< 3.99.0), gambas3-gb-openssl (>= 3.16), gambas3-gb-openssl (<< 3.99.0), gambas3-gb-pcre (>= 3.16), gambas3-gb-pcre (<< 3.99.0)
Package-List:
 glom-browser deb contrib/web optional arch=all
Checksums-Sha1:
 25a4a8c58c59bda43c0bb0c8a9b0c032b60a95f2 102019222 glom-browser_0.0.186.orig.tar.gz
 960464961d15d5189c00fc9e54f21ad6f9157edd 2350 glom-browser_0.0.186-1.diff.gz
Checksums-Sha256:
 3cc67717afc8149d3d3d52a0aa6cf8bc6a9bbdd04b80f8dd59c14987082ccb1f 102019222 glom-browser_0.0.186.orig.tar.gz
 c007becb7903551d1dd612d4f577ce7dd235f074dfd4bbf6d551cdc7955ad62c 2350 glom-browser_0.0.186-1.diff.gz
Files:
 53439029bc5e699fdab609371a432feb 102019222 glom-browser_0.0.186.orig.tar.gz
 4986dccabe591faef3f7866d3352b801 2350 glom-browser_0.0.186-1.diff.gz
