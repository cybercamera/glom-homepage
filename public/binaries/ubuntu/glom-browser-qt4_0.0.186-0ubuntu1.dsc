Format: 1.0
Source: glom-browser-qt4
Binary: glom-browser-qt4
Architecture: all
Version: 0.0.186-0ubuntu1
Maintainer: Con Zymaris <cybercamera@gmail.com>
Standards-Version: 3.9.6
Build-Depends: debhelper (>= 9)
Package-List:
 glom-browser-qt4 deb contrib/web optional arch=all
Checksums-Sha1:
 b23c61701679277edb7d2dd4c6b7c4f8d7ef9e03 1421 glom-browser-qt4_0.0.186-0ubuntu1.tar.gz
Checksums-Sha256:
 23796011544dd3da91f68aaefd8df62273c75f22ecd0ae741f86ec2f7bcc1959 1421 glom-browser-qt4_0.0.186-0ubuntu1.tar.gz
Files:
 fa950c2eb1bb46278f549836d9f6903b 1421 glom-browser-qt4_0.0.186-0ubuntu1.tar.gz
