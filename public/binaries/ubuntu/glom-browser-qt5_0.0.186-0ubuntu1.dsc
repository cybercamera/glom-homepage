Format: 1.0
Source: glom-browser-qt5
Binary: glom-browser-qt5
Architecture: all
Version: 0.0.186-0ubuntu1
Maintainer: Con Zymaris <cybercamera@gmail.com>
Standards-Version: 3.9.6
Build-Depends: debhelper (>= 9)
Package-List:
 glom-browser-qt5 deb contrib/web optional arch=all
Checksums-Sha1:
 7cef93d8d5a576236e39519c4dc59bcf459dd2a3 1422 glom-browser-qt5_0.0.186-0ubuntu1.tar.gz
Checksums-Sha256:
 fff6509cbd16de3fe3a97f21e9d4ad79afe0aad885afbaf3a671c71756d7c4c3 1422 glom-browser-qt5_0.0.186-0ubuntu1.tar.gz
Files:
 b933d9cd30c4b7861b3d2ecc99584f0d 1422 glom-browser-qt5_0.0.186-0ubuntu1.tar.gz
