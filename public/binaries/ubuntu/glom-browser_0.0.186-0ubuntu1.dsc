Format: 1.0
Source: glom-browser
Binary: glom-browser
Architecture: all
Version: 0.0.186-0ubuntu1
Maintainer: Con Zymaris <cybercamera@gmail.com>
Standards-Version: 3.9.6
Build-Depends: debhelper (>= 9), gambas3-dev (>= 3.16), gambas3-dev (<< 3.99.0), gambas3-gb-args (>= 3.16), gambas3-gb-args (<< 3.99.0), gambas3-gb-image (>= 3.16), gambas3-gb-image (<< 3.99.0), gambas3-gb-form (>= 3.16), gambas3-gb-form (<< 3.99.0), gambas3-gb-data (>= 3.16), gambas3-gb-data (<< 3.99.0), gambas3-gb-db (>= 3.16), gambas3-gb-db (<< 3.99.0), gambas3-gb-db-form (>= 3.16), gambas3-gb-db-form (<< 3.99.0), gambas3-gb-db-sqlite3 (>= 3.16), gambas3-gb-db-sqlite3 (<< 3.99.0), gambas3-gb-desktop (>= 3.16), gambas3-gb-desktop (<< 3.99.0), gambas3-gb-settings (>= 3.16), gambas3-gb-settings (<< 3.99.0), gambas3-gb-term (>= 3.16), gambas3-gb-term (<< 3.99.0), gambas3-gb-form-terminal (>= 3.16), gambas3-gb-form-terminal (<< 3.99.0), gambas3-gb-net (>= 3.16), gambas3-gb-net (<< 3.99.0), gambas3-gb-net-curl (>= 3.16), gambas3-gb-net-curl (<< 3.99.0), gambas3-gb-markdown (>= 3.16), gambas3-gb-markdown (<< 3.99.0), gambas3-gb-openssl (>= 3.16), gambas3-gb-openssl (<< 3.99.0), gambas3-gb-pcre (>= 3.16), gambas3-gb-pcre (<< 3.99.0)
Package-List:
 glom-browser deb contrib/web optional arch=all
Checksums-Sha1:
 0caa603975e30b759407366679152a2c25483439 102019235 glom-browser_0.0.186.orig.tar.gz
 86ff57457bde98c28bd54ed26d563c163d683a2f 2357 glom-browser_0.0.186-0ubuntu1.diff.gz
Checksums-Sha256:
 73d0f802ea466e66d6bdac78ec7873945666a423fc9b5927680a11180cedd577 102019235 glom-browser_0.0.186.orig.tar.gz
 4fa75e966f8899934090cba663dfe2abf76ab33506bb06da92b6975ae978b1d9 2357 glom-browser_0.0.186-0ubuntu1.diff.gz
Files:
 f638c58922c6fdccc09eb115b83a841c 102019235 glom-browser_0.0.186.orig.tar.gz
 e9376e39917f738c234c02e57b890566 2357 glom-browser_0.0.186-0ubuntu1.diff.gz
